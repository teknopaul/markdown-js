#!/usr/bin/env node

/**
 * Builds a Chapter, same as markbookBook but nested one directory lower
 */

var fs = require('fs');
var path = require('path');
var logger = require("../lib/logger.js").logger;
var File = require("../lib/File.js").File;
var markbookLib = require("../lib/Markbook.js");

if (process.argv.length < 4 ) {
	logger.error(1,"src and dest paths required");
	process.exit(1);
}

var srcPath = new File(process.argv[2]).getAbsolutePath();
var destPath = new File(process.argv[3]).getAbsolutePath();

logger.info(2, "Generating chapter " + srcPath);

var markbook = new markbookLib.Markbook();

// page defaults to the book's setup.
var page = new markbookLib.MarkbookPage();
page.bookPath =     srcPath + "/..";
page.chapterPath =  srcPath;
page.headerPath =   srcPath + "/../.head.md";
page.footerPath =   srcPath + "/../.footer.md";
page.headPath =     srcPath + "/../.head.html";
page.tailPath =     srcPath + "/../.tail.html";

// page is overwritten by the chapter
if ( fs.existsSync(srcPath + "/.head.md") )   page.headerPath = srcPath + "/.head.md";
if ( fs.existsSync(srcPath + "/.footer.md") ) page.footerPath = srcPath + "/.footer.md";
if ( fs.existsSync(srcPath + "/.index.md") )  page.markdownPath = srcPath + "/.index.md";
if ( fs.existsSync(srcPath + "/.head.html") ) page.headPath = srcPath + "/.head.html";
if ( fs.existsSync(srcPath + "/.head.html") ) page.tailPath = srcPath + "/.head.html";

if ( ! fs.existsSync(page.headPath) ) page.headPath = "../lib/html/head.html";
if ( ! fs.existsSync(page.tailPath) ) page.tailPath = "../lib/html/tail.html";

var toc = [];
var tocPath = destPath + "/toc.html";

// load the books meta data (if there is any)

var chapterAtts = markbook.getAllAttributes(srcPath + "/..", srcPath);

// Write the index page

if ( fs.existsSync(srcPath + "/.index.html") ) {
	fs.copyFileSync(srcPath + "/.index.html", destPath + "/index.html");
}
else if ( fs.existsSync(srcPath + "/.index.md") ) {
	
	page.markdownPath = srcPath + "/.index.md"
	
	markbook.writePage(page, destPath + "/index.html");
	
}
else if ( fs.existsSync(srcPath + "/.chapter.mb") ) {

	// Create HTML from .chapter.md
	var html = '<h1 class="chaptertitle">' + markbook.getTitle( srcPath + "/.chapter.mb" ) + '</h1>';

	if (chapterAtts.author) html += '<h2 class="author">by ' + chapterAtts.author + '</h2>';

	html += '<p class="chaptersynposis">' + markbook.getIntro( srcPath + "/.chapter.mb" ) + '</p>';

	html += '<div><a href="toc.html">Table of contents</a></div>';

	page.htmlText = html;
	
	var atts = {};
	atts[".chapter.md_css"] = "selected";
	
	markbook.writePage(page, destPath + "/index.html", atts);

}
else {
	tocPath = destPath + "/index.html";
}

//  convert Markdown files and populate toc
var files = fs.readdirSync(srcPath);

for ( var i = 0 ; i < files.length ; i++ ) {

	var absPath = srcPath + '/' + files[i];
	page.clearData();
	
	if ( fs.statSync(absPath).isFile() ) {
		var name = files[i];
		if (".header.md" === name || ".footer.md" === name) continue;
		
		var tocEntry = {};
		var addtoToc = ! (name.indexOf("_") === 0);
		if ( name.indexOf(".md") === name.length - 3) {
			var destFile = new File(destPath + "/" + new File(name).ext("html").name());
			if (name.indexOf(".") === 0) {
 				destFile = new File(destPath + "/" + new File(destFile).name().substring(1));
				addtoToc = false;
			}
			page.markdownPath = absPath;
			var atts = {};
			atts[name + "_css"] = "selected";
			markbook.writePage(page, destFile.getFullPath(), atts);
			
			if (addtoToc) {
				tocEntry.dest = destFile.name();
				tocEntry.title = markbook.getTitle(absPath);
				tocEntry.synopsis = markbook.getIntro(absPath);
				toc.push(tocEntry);
			}
		}
	}
}

// Process the overriden files
var files = fs.readdirSync(srcPath);

for ( var i = 0 ; i < files.length ; i++ ) {
	var absPath = srcPath + '/' + files[i];
	if ( fs.statSync(absPath).isFile() ) {
		var name = new File(absPath).name();
		if (name.indexOf(".") === 0 && name.indexOf(".html") === name.length - 5) {
			var destFile = new File(destPath + "/" + name.substring(1));
			fs.copyFileSync(absPath, destFile.getFullPath());
		}
	}
}

page.clearData();

var atts = markbook.cloneAttributes(chapterAtts);
atts[process.argv[1] + "_css"] = "selected";

markbook.writeToc(page, toc, tocPath, atts);


