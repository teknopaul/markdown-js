#!/usr/bin/env node
/*
# Filter replacing @param@
#
# Takes two args the root path of the book and the root path of the chapter if working on files in a chapter.
#
# Also accepts name:value  pairs on the command line after chapter containing any additional attributes.
*/
var fs = require('fs');

var markbookLib = require("../lib/Markbook.js");

if ( process.argv.length < 3) {
	console.error("filter invalid args");
	process.exit(1);
}


var markbook = new markbookLib.Markbook();

var atts = markbook.getAllAttributes(process.argv[2], process.argv[3]);
//for (att in atts)
//	stderr.writeString("here " + att +":" + atts[att] + "\n");

if (process.argv.length > 3) {
	for (var i = 4 ; i < process.argv.length ; i++) {
		var colonIdx = process.argv[i].indexOf(":");
		if (colonIdx > 0) {
			atts[process.argv[i].substring(0, colonIdx)] = process.argv[i].substring(colonIdx + 1);
		}
	}
}

function replace(string, atts) {
	var buffer = string;
	for ( att in atts ) {
		buffer = buffer.replace("@" + att + "@", atts[att]); 
	}
	return buffer;
}

var stdinBuffer = "" + fs.readFileSync(0);
var lines = stdinBuffer.split('\n');
for (var i = 0 ; i < lines.length ; i++) {
	var line = lines[i];
	if ( line.indexOf("@") >= 0 ) {
		console.log( replace(line, atts) );
	}
	else {
		console.log(line);
	}
}
