#!/usr/bin/env node

var fs = require('fs');
var path = require('path');
var child_process = require('child_process');
var logger = require("../lib/logger.js").logger;
var File = require("../lib/File.js").File;

var i = 0;
binjs_exec = function(command) {
	var tmpScript = "/tmp/" + process.pid + "-" + ++i + "-tmp-mbjs-exec.sh";
	fs.writeFileSync(tmpScript, command);
	try {
		child_process.execFileSync('/bin/bash', [tmpScript], options);
		fs.unlinkSync(tmpScript);
	} catch (err) {
		logger.error(9, "exception running: " + command);
	}
}


if (process.argv.length <4 ) {
	logger.error(1,"src and dest paths required");
	process.exit(1);
}

var srcPath = new File(process.argv[2]).getAbsolutePath();
var destPath = new File(process.argv[3]).getAbsolutePath();
DEST_PATH = destPath;

logger.info(1, "Generating book", srcPath);

var markbookLib = require("../lib/Markbook.js");

var markbook = new markbookLib.Markbook();

// make the book's index page
var page = new markbookLib.MarkbookPage();
page.bookPath = srcPath;
page.headerPath = srcPath + "/.head.md";
page.footerPath = srcPath + "/.footer.md";
page.markdownPath = srcPath + "/.index.md"
page.headPath =  srcPath + "/.head.html";
page.tailPath = srcPath + "/.tail.html";

if ( ! fs.existsSync(page.headPath) ) page.headPath = "../lib/html/head.html";
if ( ! fs.existsSync(page.tailPath) ) page.tailPath = "../lib/html/tail.html";

// list of tocEntry objects (relative paths)
var toc = [];
var tocPath = destPath + "/toc.html";

// load the books meta data (if there is any)
var bookAttributes = markbook.getBookAttributes(srcPath);

if ( fs.existsSync(srcPath + "/.index.html") ) {
	fs.copyFileSync(srcPath + "/.index.html", destPath + "/index.html");
}
else if (fs.existsSync(srcPath + "/.index.md")) {
	
	// will be generated below as a normal file 
	//page.markdownPath = srcPath + "/.index.md"
	//markbook.writePage(page, destPath + "/index.html", {".index.md_css":"selected"});
	
}
else if ( fs.existsSync(srcPath + "/.book.mb") ) {
	// Create HTML from .book.md
	var html = '<h1 class="booktitle">' + markbook.getTitle( srcPath + "/.book.mb" ) + '</h1>';

	if (bookAttributes.author) html += '<h2 class="author">by ' + bookAttributes.author + '</h2>';

	html += '<p class="booksynposis">' + markbook.getIntro( srcPath + "/.book.mb" ) + '</p>';

	html += '<div><a href="toc.html">Table of contents</a></div>';
	
	page.htmlText = html;
	
	var atts = {};
	atts[".index.md_css"] = "selected";
	
	markbook.writePage(page, destPath + "/index.html", atts);
	
}
else {
	tocPath = destPath + "/index.html";
}


//  convert Markdown files and populate toc
var files = fs.readdirSync(srcPath);

for ( var i = 0 ; i < files.length ; i++ ) {

	var absPath = srcPath + '/' + files[i];
	var name = files[i];
	page.clearData();
	
	// process markdown files
	if ( fs.statSync(absPath).isFile() ) {

		if (".header.md" === name || ".footer.md" === name) continue;
		
		var tocEntry = {};
		var addtoToc = ! (name.indexOf("_") == 0); // dont add files starting with _ to the toc.
		if (name.indexOf(".md") == name.length - 3) {
			var destFile = new File(destPath + "/" + new File(name).ext("html").name());
			if (name.indexOf(".") == 0) {
				destFile = new File(destPath + "/" + destFile.name().substring(1));
				addtoToc = false;
			}
			page.markdownPath = absPath;
			var atts = {};
			atts[name + "_css"] = "selected";
			markbook.writePage(page, destFile.getFullPath(), atts);
			
			if (addtoToc) {
				tocEntry.dest = destFile.name();
				tocEntry.title = markbook.getTitle(absPath);
				tocEntry.synopsis = markbook.getIntro(absPath);
				toc.push(tocEntry);
			}
		}
	}

	// process chapter directories
	if ( fs.statSync(absPath).isDirectory() ) {
		if (new File(absPath).name().indexOf(".") == 0) continue;
		
		var tocEntry = {};
		var sectionMetaFile = new File(absPath + "/.chapter.mb");
		if ( fs.existsSync(sectionMetaFile.getFullPath()) ) {
			tocEntry.dest = name + "/index.html";
			tocEntry.title = markbook.getTitle(sectionMetaFile.getFullPath());
			tocEntry.synopsis = markbook.getIntro(sectionMetaFile.getFullPath());
			toc.push(tocEntry);
		}
		else if ( new File(absPath).dirContainsMarkdown() ) {
			tocEntry.dest = name + "/index.html";
			tocEntry.title = name;
			tocEntry.synopsis = "";
			toc.push(tocEntry);
		}

		chSrc = new File(absPath).getAbsolutePath();
		chDest =  destPath + "/" + new File(absPath).name();
		try {
			fs.mkdirSync(chDest, {recursive: true});
		} catch (err) {
		}
		binjs_exec("./markbookChapter.js '" + chSrc + "' '" + chDest + "'");
	}

}

// Process the override files (html files that start with a . e.g. .toc.html)
var files = fs.readdirSync(srcPath);

for ( var i = 0 ; i < files.length ; i++ ) {
	var absPath = srcPath + '/' + files[i];
	if ( fs.statSync(absPath).isFile() ) {
		var name = new File(absPath).name();
		if (name.indexOf(".") === 0 && name.indexOf(".html") === name.length - 5) {
			var destFile = new File(destPath + "/" + name.substring(1));
			fs.copyFileSync(absPath, destFile.getFullPath());
		}
	}
}

page.clearData();
var atts = {};
atts["index_toc_css"] = "selected";
markbook.writeToc(page, toc, tocPath, atts);

logger.info(3, "Copying static content");

try {
	fs.mkdirSync( destPath + "/css",  { recursive: true });
} catch (err) {}

if ( fs.existsSync(srcPath + "/" + ".css") ) {
	binjs_exec("cp -R --archive " + srcPath + "/.css/* " + destPath+"/css");
}
else {
	binjs_exec("cp -R --archive ../lib/css/* " + destPath + "/css");
}

try {
	fs.mkdirSync(destPath + "/img",  { recursive: true });
} catch (err) {
}

if ( fs.existsSync(srcPath + "/" + ".img") ) {
	binjs_exec("cp -R --archive " + srcPath + "/.img/* " + destPath + "/img");
}
else {
	binjs_exec("cp -R --archive ../lib/img/* " + destPath + "/img");
}


