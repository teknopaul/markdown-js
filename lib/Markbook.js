

var fs = require('fs');
var child_process = require('child_process');
var logger = require("./logger.js").logger;

var options = {
	env : {}
}

setEnv = function(name, value) {
	options.env[name] = value;
}

var i = 0;

binjs_exec = function(command) {
	var tmpScript = "/tmp/" + process.pid + "-" + ++i + "-tmp-mb-exec.sh";
	fs.writeFileSync(tmpScript, command);
	try {
		child_process.execFileSync('/bin/bash', [tmpScript], options);
		fs.unlinkSync(tmpScript);
	} catch (err) {
		// no- szero return 
		logger.error(10, "exception running: " + command + " " + tmpScript);
		process.exit(2);
	}
}

function append(string, file) {
	fs.writeFileSync(file, string, {flag: 'a'});
}


exports.Markbook = function() {

}

/**
 * Write out an HTML page, from HTML text or a Markdown file.
 */
exports.Markbook.prototype.writePage = function(page, destPath, atts) {

	var date = new Date().toUTCString();
	setEnv("PAGE_PATH", destPath);
	
	binjs_exec('cat ' + page.headPath 
		+ ' | ./filter.js ' + page.bookPath + ' ' + (page.chapterPath ? page.chapterPath : '""') 
		+ ' "date:' + date + '"'
		+ ' "markdownPath:' + page.markdownPath + '"'
		+ this._formatAtts(atts)
		+ ' > ' + destPath);
	
	if ( fs.existsSync(page.headerPath)) {
		setEnv("F_PATH", page.headerPath);
		binjs_exec('perl ../lib/Markdown.pl $F_PATH >> ' + destPath);
	}

	setEnv("F_PATH", page.markdownPath);
	if (page.markdownPath) {
		binjs_exec('perl ../lib/Markdown.pl $F_PATH >> ' + destPath);
	}
	else {
		setEnv("DATA", page.htmlText);
		append(page.htmlText, destPath);
	}
	
	if ( fs.existsSync(page.footerPath)) {
		setEnv("F_PATH", page.footerPath);
		binjs_exec('test -f $F_PATH && perl ../lib/Markdown.pl $F_PATH >> ' + destPath);
	}
	
	binjs_exec('cat ' + page.tailPath 
		+ ' | ./filter.js ' + page.bookPath + ' ' + (page.chapterPath ? page.chapterPath : '""') 
		+ ' "date:' + date + '"'
		+ ' "markdownPath:' + page.markdownPath + '"'
		+ this._formatAtts(atts)
		+ ' >> ' + destPath);
	
	logger.info(5,"Generated " + destPath);
}

/**
 * Write the table of contents which lists files and or subdirectories 
 */
exports.Markbook.prototype.writeToc = function(page, toc, tocPath, atts) {

	var date = new Date().toUTCString();
	setEnv("TOC_PATH", tocPath);

	binjs_exec('cat ' + page.headPath
		+ ' | ./filter.js ' + page.bookPath + ' ' + (page.chapterPath ? page.chapterPath : '""') 
		+ ' "date:' + date + '"'
		+ this._formatAtts(atts)
		+ ' > "$TOC_PATH"');
	
	append("<h1>Table of contents</h1>\n", tocPath);
	
	append("<ul class=\"markbooktoc\">\n", tocPath);
		
	for (var i = 0 ; i < toc.length ; i++) {

		append('<li class="idx"><a href="' + toc[i].dest + '">' + toc[i].title + '</a> - ' + toc[i].synopsis + '</li>\n', tocPath);
	
	}
	append("</ul>\n", tocPath);
	
	binjs_exec('cat ' + page.tailPath 
		+ ' | ./filter.js ' + page.bookPath + ' ' + (page.chapterPath ? page.chapterPath : '""') 
		+ ' "date:' + date + '"'
		+ this._formatAtts(atts)
		+ '  >> "$TOC_PATH"');
	
	logger.info(6,"Generated toc " + tocPath);
}

/**
 * Get the first H1 header
 */
exports.Markbook.prototype.getTitle = function(file) {
	var lines = ("" + fs.readFileSync(file)).split('\n');
	for (var i = 0 ; i < lines.length ; i++) {
		var line = lines[i];
		if ( line.indexOf("# ") === 0 ) {
			return line.substring(2);
		}
	}
	return file.name;
}

/**
 * Return the first bit of markdown text, up to the first blank line
 * after the # header row.
 */
exports.Markbook.prototype.getIntro = function(file) {
	var lines = ("" + fs.readFileSync(file)).split('\n');
	var intro = "";
	var saving = false;
	for (var i = 0 ; i < lines.length ; i++) {
		var line = lines[i];
		if ( line.indexOf("# ") === 0 ) {
			saving = true;
		}
		else if (saving === true) {
			if ( line.trim().length === 0 && intro.length !== 0 ) {
				return intro.trim();
			}
			else {
				intro +=  line + " ";
			}
		}
	}
	return intro.trim();
}

/**
 * Gets the attributes from a .mb file.
 */
exports.Markbook.prototype.getAttributes = function(filedata) {
	var atts = {};
	var lines = filedata.split('\n');
	for ( var i = 0 ; i < lines.length ; i++ ) {
		if ( /^[a-zA-Z0-9-_]+:/.test(lines[i]) ) {
			var colonIdx = lines[i].indexOf(":");
			atts[lines[i].substring(0, colonIdx).trim()] = lines[i].substring(colonIdx + 1).trim();
		}
	}
	return atts;
}

exports.Markbook.prototype.getBookAttributes = function(srcPath) {
	var bookMetaFile = srcPath + "/.book.mb";
	if ( fs.existsSync(bookMetaFile) ) {
		return this.getAttributes("" + fs.readFileSync(bookMetaFile));
	}
	else return {};
}

exports.Markbook.prototype.getChapterAttributes = function(srcPath) {
	var chapterMetaFile = srcPath + "/.chapter.mb";
	if ( fs.existsSync(chapterMetaFile) ) {
		return this.getAttributes("" + fs.readFileSync(chapterMetaFile));
	}
	else return {};
}
/**
 * Merge all attributes for a file, this is achieved by taking the 
 * .book.mb attributes and adding and overriding any .chapter.mb attributes
 * then adding and overriding any .MyPage.mb attributes
 */
exports.Markbook.prototype.getAllAttributes = function(bookPath, chapterPath) {

	var atts = this.getBookAttributes(bookPath);
	atts.root = "./";
	atts.path = bookPath;
	
	if ( chapterPath ) {  
		// override book atts with chapter atts
		var chapterAttributes = this.getChapterAttributes(chapterPath);
		for ( att in chapterAttributes) {
			atts[att] = chapterAttributes[att];
		}
		atts.root = "../";
		atts.path = chapterPath;
	}
	return atts;
}

exports.Markbook.prototype.cloneAttributes = function(origAtts) {
	var atts = {};
	for ( att in origAtts) {
		atts[att] = origAtts[att];
	}
	return atts;
}
/**
 * Markup just bold and code, * and `
 */
exports.Markbook.prototype._markupIntro = function(text) {
	var buffer = "";
	var inCode = false;
	var inBold = false;
	for (var i = 0 ; i < text.length ; i++) {
		var c = text.charAt(i);
		if ('*' == c) {
			if ( inCode ) {
				buffer += c;
			}
			else {
				buffer += (inBold ? "</b>" : "<b>");
				inBold = !inBold;
			}
		}
		if ('`' == c) {
			buffer += (inCode ? "</code>" : "<code>");
			inCode = !inCode;
		}
		else buffer += c;
	}
	return buffer;
}

exports.Markbook.prototype._formatAtts = function(atts) {
	if (! atts) return "";
	
	var buffer = "";
	for (att in atts) {
		buffer += (' "' + att + ':' + atts[att] + '"');
	}
	return buffer;
}

/*
 * Object represents a page in the book
 * All attributes should be strings containing absolute paths
 */
exports.MarkbookPage = function() {
	this.bookPath;
	this.chapterPath;
	// optional
	this.headerPath;
	this.footerPath;
	// one of
	this.markdownPath;
	this.htmlText;
	// required
	this.headPath;
	this.tailPath;
}
exports.MarkbookPage.prototype.clearData = function() {
	this.markdownPath = undefined;
	this.htmlText = undefined;
} 
