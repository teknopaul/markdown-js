var fs = require('fs');
var path = require('path');

/**
 * File util in the style of java.io.File
 */

var File = function(p) {
	this.path = p;
}
// make a real relative path an absolute path
File.prototype.getAbsolutePath = function() {
	return fs.realpathSync(this.path);
}
// get an absolute path when path might not exist, but we already know its absolute
File.prototype.getFullPath = function() {
	return this.path;
}
File.prototype.name = function() {
	return path.basename(this.path);
}
File.prototype.ext = function(newValue) {

	var lastDot = this.path.lastIndexOf('.');

	if (typeof newValue === 'undefined') {

		// fetch the extension
		if (lastDot === -1) {
			return null;
		}
		
		// hidden files  like .bashrc are not really a
		// extensions in my book, ignore them completly
		if (lastDot === 0) {
			return null;
		}

		return this.path.substring(lastDot + 1);
	}
	else {
	
		// set the extension (manipulate path)
		
		// if it had not ext, or was a hidden file, just add it
		if (lastDot <= 0) {
			if (newValue.charAt(0) != '.') {
				newValue = '.' + newValue;
			}
			return new File( this.path + newValue);
		}

		// else manipulate it
		lastDot = this.path.lastIndexOf('.');
		if (lastDot <= 0)lastDot = this.path.length;
		
		if (newValue.length == 0) {
			return new File( this.path.substring(0, lastDot) );
		} else {
			if (newValue.charAt(0) != '.') {
				newValue = '.' + newValue;
			}
			return new File( this.path.substring(0, lastDot) + newValue );
		}
	}
}
File.prototype.dirContainsMarkdown = function() {
	var files = fs.readdirSync(this.path);
	for ( var i = 0 ; i < files.length ; i++ ) {
		if (files[i].endsWith(".md")) return true;
	}
	return false;
}

exports.File = File;
