# Markbook-js
![markbook](handbook/.img/markbook-48x48.png)

Markbook is a couple of scripts used to convert Markdown files into an HTML book with cover page and table of contents.

No installation is needed for Markbook, just checkout the code and run the `bin/markbook` script, but you do need `nodejs`, `perl` & `markdown.pl` installed.

[Markbook handbook](http://markbook.tp23.org/handbook/) | 
[Markbook with no metadata](http://markbook.tp23.org/simple/)


