PREFIX ?= /bin

all:
	mkdir -p simple_output handbook_output
	bin/markbook simple simple_output
	bin/markbook handbook handbook_output

deb:
	sudo deploy/builddeb.sh

test:
	cd test; node test-markbook.js

clean:
	rm -f markbook-*-1.noarch.deb 
	rm -rf simple_output/* handbook_output/*


.PHONY: all clean test
