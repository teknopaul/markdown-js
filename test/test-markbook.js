var assert = require("assert");

var markbookLib = require("../lib/Markbook.js");

var markbook = new markbookLib.Markbook();

assert.equal("The Simplest Markbook", markbook.getTitle("./in/JustMarkdown.md"));

assert.equal("This Markbook consists of just Markdown files with no chapters and no meta data.", markbook.getIntro("./in/JustMarkdown.md"));

assert.equal("GPLv3", markbook.getBookAttributes("./in").copyright);

assert.equal("GPLv3", markbook.getChapterAttributes("./in").copyright);

